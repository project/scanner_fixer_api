<?php

namespace Drupal\scanner_fixer_api\Scanner;

/**
 * An interface for objects that scan for problems.
 */
interface ScannerInterface {

  /**
   * Find a list of items with problems.
   *
   * @return array
   *   An array of item IDs, whose key and value in every row is the item ID. If
   *   no problem IDs are found, this will be empty.
   */
  public function findProblemIds();

}
