<?php

namespace Drupal\scanner_fixer_api\Fixer;

/**
 * An interface for objects that can fix problem items.
 */
interface FixerInterface {

  /**
   * Whether this fixer can fix the item.
   *
   * @param mixed $item
   *   The item to check.
   *
   * @return bool
   *   TRUE if this fixer can fix the item. FALSE if it cannot.
   */
  public function canFix($item);

  /**
   * Actually fix the item.
   *
   * @param mixed $item
   *   The item to fix.
   *
   * @return bool
   *   TRUE if this fixer successfully fixed the item; FALSE otherwise.
   *
   * @throws \Exception
   *   Attempting to perform the fix may result in some sort of Exception. The
   *   exact Exception thrown would be determined by the Fixer itself.
   */
  public function performFix($item);

}
