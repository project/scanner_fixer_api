<?php

namespace Drupal\test_scanner_fixer\Scanner;

use Drupal\scanner_fixer_api\Scanner\ScannerInterface;

/**
 * A scanner that finds one item to fix.
 */
class OneItemsScanner implements ScannerInterface {

  /**
   * {@inheritdoc}
   */
  public function findProblemIds() {
    $id = rand();
    return array($id => $id);
  }

}
