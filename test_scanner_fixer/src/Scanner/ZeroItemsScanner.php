<?php

namespace Drupal\test_scanner_fixer\Scanner;

use Drupal\scanner_fixer_api\Scanner\ScannerInterface;

/**
 * A scanner that finds nothing to fix.
 */
class ZeroItemsScanner implements ScannerInterface {

  /**
   * {@inheritdoc}
   */
  public function findProblemIds() {
    return array();
  }

}
