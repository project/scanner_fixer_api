<?php

namespace Drupal\test_scanner_fixer\Scanner;

use Drupal\scanner_fixer_api\Scanner\ScannerInterface;

/**
 * A scanner that finds five items to fix.
 */
class FiveItemsScanner implements ScannerInterface {

  /**
   * {@inheritdoc}
   */
  public function findProblemIds() {
    $ids = array();

    for ($i = 0; $i < 5; $i++) {
      $id = rand();
      $ids[$id] = $id;
    }

    return $ids;
  }

}
