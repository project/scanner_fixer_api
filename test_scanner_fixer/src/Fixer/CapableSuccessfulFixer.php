<?php

namespace Drupal\test_scanner_fixer\Fixer;

use Drupal\scanner_fixer_api\Fixer\FixerInterface;

/**
 * A fixer that says it can fix an item, then does so successfully.
 */
class CapableSuccessfulFixer implements FixerInterface {

  /**
   * {@inheritdoc}
   */
  public function canFix($item) {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function performFix($item) {
    return TRUE;
  }

}
