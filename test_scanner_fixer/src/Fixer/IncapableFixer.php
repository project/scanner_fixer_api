<?php

namespace Drupal\test_scanner_fixer\Fixer;

use Drupal\scanner_fixer_api\Fixer\FixerInterface;
use LogicException;

/**
 * A fixer that says it cannot fix the item.
 */
class IncapableFixer implements FixerInterface {

  /**
   * {@inheritdoc}
   */
  public function canFix($item) {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function performFix($item) {
    throw new LogicException('This line of code should never be executed.');
  }

}
