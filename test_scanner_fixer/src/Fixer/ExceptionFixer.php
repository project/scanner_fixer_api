<?php

namespace Drupal\test_scanner_fixer\Fixer;

use Drupal\scanner_fixer_api\Fixer\FixerInterface;
use Exception;

/**
 * A fixer that throws an exception when trying to fix the item.
 */
class ExceptionFixer implements FixerInterface {

  /**
   * {@inheritdoc}
   */
  public function canFix($item) {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function performFix($item) {
    throw new Exception('Message.', 314);
  }

}
