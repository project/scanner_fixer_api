<?php

namespace Drupal\test_scanner_fixer\Fixer;

use Drupal\scanner_fixer_api\Fixer\FixerInterface;

/**
 * A fixer that says it can fix an item, but fails to follow through with that.
 */
class CapableFailureFixer implements FixerInterface {

  /**
   * {@inheritdoc}
   */
  public function canFix($item) {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function performFix($item) {
    return FALSE;
  }

}
