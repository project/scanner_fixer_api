<?php

/**
 * @file
 * API functions for the scanner_fixer_api module.
 */

/**
 * A function to register scanner-fixer solutions.
 *
 * A "solution" is an association of scanner(s) and fixer(s) intended to work
 * together.
 *
 * @return array
 *   An associative array of solutions. The keys of the array are machine names
 *   for solutions, and each corresponding value is itself an associative array
 *   with the following key-value pairs:
 *   - title: A human-friendly name for this solution.
 *   - description: A longer description for this solution.
 *   - scanners: An array of scanner objects to use in this solution.
 *   - fixers: An array of fixer objects to use in this solution.
 */
function hook_scanner_fixer_api_solution_info() {
  $solutions = array();

  // An example with values that describe what they are for.
  $solutions['machine_name'] = array(
    'title' => t('A human-friendly name for this operation.'),
    'description' => t('A longer description for this operation.'),
    'load_item_callback' => 'scanner_fixer_api__null_load_item_callback',
    'scanners' => array(
      // Calls to construct zero or more objects that implement
      // \Drupal\scanner_fixer_api\Scanner\ScannerInterface.
    ),
    'fixers' => array(
      // Calls to construct zero or more objects that implement
      // \Drupal\scanner_fixer_api\Scanner\FixerInterface.
    ),
  );

  // Another, more-realistic example.
  $solutions['duplicate_term'] = array(
    'title' => t('Duplicate taxonomy term solver'),
    'description' => t('A solver to detect taxonomy terms with the same name and merge them.'),
    'load_item_callback' => 'node_load',
    'scanners' => array(
      new \Drupal\test_scanner_fixer\Scanner\ZeroItemsScanner(),
    ),
    'fixers' => array(
      new \Drupal\test_scanner_fixer\Fixer\CapableSuccessfulFixer(),
    ),
  );

  return $solutions;
}
